## Mail Service Package
This is the PHP wrapper for the Venuzle Mail Service API.

### Sending an email using the default template
```
use Venuzle\MailService;

$appKey = 'exampleKey';
$mailService = new MailService($appKey);

// Configure the email details
$mailService->setSubject('Hello!')
    ->setTo('example@venuzle.at')
    ->setFrom('manager@venuzle.at')
    ->setReplyTo('manager@venuzle.at')
    ->setTemplate('default_basic', [
        'content' => 'Just a test mail.',
    ])
    ->setExternalIdentifier('partner_code')
    ->send()
;

// Check for errors
if ($mailService->hasErrors()) {
    $errors = $mailService->getErrors();
}

```
