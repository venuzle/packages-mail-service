<?php

namespace Venuzle;

class MailService {

    /**
     * @var string
     */
    private $appKey;

    /**
     * @var string
     */
    private $subject;

    /**
     * @var string|array
     */
    private $to;

    /**
     * @var string
     */
    private $fromAddress;

    /**
     * @var string
     */
    private $fromName;

    /**
     * @var string
     */
    private $replyTo;

    /**
     * @var string|array
     */
    private $bcc;

    /**
     * @var string
     */
    private $templateSlug;

    /**
     * @var array
     */
    private $templateData;

    /**
     * @var string
     */
    private $externalIdentifier;

    /**
     * @var array
     */
    private $errors = [];

    /**
     * @var string
     */
    private $productionApi = 'https://mail.services.venuzle.com/';

    /**
     * @var string
     */
    private $stagingApi = 'http://mail.services.staging.venuzle.com/';

    /**
     * If enabled, the staging api will be used.
     *
     * @var boolean
     */
    private $developmentMode = false;

    /**
     * @param string  $app_key
     * @param boolean $developmentMode
     */
    public function __construct($appKey, $developmentMode = false)
    {
        $this->appKey = $appKey;

        if ($developmentMode) {
            $this->developmentMode = true;
        }
    }

    /**
     * @param string $subject
     */
    public function setSubject($subject)
    {
        $this->subject = $subject;
        return $this;
    }

    /**
     * @param string|array $to
     */
    public function setTo($to)
    {
        $this->to = $to;
        return $this;
    }

    /**
     * @param string $fromAddress
     * @param string $fromName
     */
    public function setFrom($fromAddress, $fromName = '')
    {
        $this->fromAddress = $fromAddress;

        if ($fromName) {
            $this->fromName = $fromName;
        }

        return $this;
    }

    /**
     * @param string $replyTo
     */
    public function setReplyTo($replyTo)
    {
        $this->replyTo = $replyTo;
        return $this;
    }

    /**
     * @param string|array $bcc
     */
    public function setBcc($bcc)
    {
        $this->bcc = $bcc;
        return $this;
    }

    /**
     * @param string $templateSlug
     * @param array  $templateData
     */
    public function setTemplate($templateSlug, $templateData = [])
    {
        $this->templateSlug = $templateSlug;
        $this->templateData = $templateData;
        return $this;
    }

    /**
     * @param string $externalIdentifier
     */
    public function setExternalIdentifier($externalIdentifier)
    {
        $this->externalIdentifier = $externalIdentifier;
        return $this;
    }

    public function addAttachment($name, $content)
    {
        $this->attachments[] = [
            'name' => $name,
            'content' => base64_encode($content),
        ];
        return $this;
    }

    /**
     * Send mail.
     */
    public function send()
    {
        $client = new \GuzzleHttp\Client;

        try {
            $response = $client->post($this->endpoint('mail'), [
                'headers' => [
                    'Accept' => 'application/json',
                ],
                \GuzzleHttp\RequestOptions::JSON => $this->buildRequestBody(),
            ]);
        } catch (\GuzzleHttp\Exception\ClientException $e) {
            $this->errors = $e->getResponse()->getBody(true)->getContents();
        }
    }

    /**
     * @return boolean
     */
    public function hasErrors()
    {
        return !empty($this->errors);
    }

    /**
     * @return array
     */
    public function getErrors()
    {
        return $this->errors;
    }

    /**
     * @return array
     */
    private function buildRequestBody()
    {
        $body = [
            'app_key' => $this->appKey,
            'from_address' => $this->fromAddress,
            'to' => $this->to,
            'subject' => $this->subject,
        ];

        if ($this->fromName) {
            $body['from_name'] = $this->fromName;
        }

        if ($this->replyTo) {
            $body['reply_to'] = $this->replyTo;
        }

        if ($this->templateSlug) {
            $body['template_slug'] = $this->templateSlug;
        }

        if ($this->templateData) {
            $body['template_data'] = $this->templateData;
        }

        if ($this->externalIdentifier) {
            $body['external_identifier'] = $this->externalIdentifier;
        }

        if ($this->attachments) {
            $body['attachments'] = [];

            foreach ($this->attachments as $attachment) {
                $body['attachments'][] = $attachment;
            }
        }

        return $body;
    }

    /**
     * @param  string $path
     * @return string
     */
    private function endpoint($path)
    {
        if ($this->developmentMode) {
            return $this->stagingApi . $path;
        }

        return $this->productionApi . $path;
    }
}
